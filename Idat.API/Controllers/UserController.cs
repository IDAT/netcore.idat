﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Idat.API.Beans;
using Idat.API.Models;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using Dapper;
// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Idat.API.Controllers
{
    [Route("api/[controller]")]
    public class UserController : Controller
    {
        //TODO - CONSTRUCTOR 
        private readonly ConnectionStringsBean oConnectionStringsBean;
        
        public UserController(ConnectionStringsBean cnx)
        {
            oConnectionStringsBean = cnx;
        }
        
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return await Task.Run(() => {
                using (var c = new MySqlConnection(oConnectionStringsBean.MySQL))
                {
                    var sql = @"SELECT * FROM tbusuarios";
                    var query = c.Query<UserModel>(sql, null, commandTimeout: 30).ToList();
                 
                    return Ok(new {status = true, message = $"{query.Count()} Registro(s) encontrado(s)", data = query });
                }
            });
        }

        [HttpGet]
        [Route("UserByCode")]
        public async Task<IActionResult> GetByCode(String userCode)
        {
            /*
            return await Task.Run(() => {
                using (var c = new MySqlConnection(oConnectionStringsBean.MySQL))
                {
                    var sql = @"SELECT * FROM tbusuarios WHERE usu_codigo=@Id";
                    var _userModel = c.QueryFirst<UserModel>(sql, new {Id = userCode }, commandTimeout: 30);
                    return Ok(new {status = true, message = $" Registro(s) encontrado(s)", data = _userModel });
                }
            });
            */
            return await Task.Run(() => {
                using (var c = new MySqlConnection(oConnectionStringsBean.MySQL))
                {
                    var sql = @"SELECT * FROM tbusuarios WHERE usu_codigo=@Id";
                    var query = c.Query<UserModel>(sql, new { Id = userCode }, commandTimeout: 30).ToList();

                    return Ok(new { status = true, message = $"{query.Count()} Registro(s) encontrado(s)", data = query });
                }
            });
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UserBean request)
        {
            return await Task.Run(() => {
                using (var c = new MySqlConnection(oConnectionStringsBean.MySQL))
                {
                    var sql = @"INSERT INTO tbusuarios
                                (usu_codigo, usu_nombre, usu_passwd, usu_descri, usu_email, usu_estcod)
                                VALUES (@usu_codigo, @usu_nombre, @usu_passwd, @usu_descri, @usu_email, @usu_estcod)";

                    int affetch = c.Execute(sql, request, commandTimeout: 30);
                     
                    return Ok(new { status = true, message = $"{affetch} Registro(s) Insertado(s)" }) ;
                }
            });
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(String userCode)
        {
            return await Task.Run(() => {
                using (var c = new MySqlConnection(oConnectionStringsBean.MySQL))
                {
                    var sql = @"DELETE FROM tbusuarios WHERE usu_codigo = @Id";
                    int affetch = c.Execute(sql, new { Id = userCode }, commandTimeout: 30);

                    return Ok(new { status = true, message = $"{affetch} Registro(s) Eliminado(s)" });
                }
            });
        }

        [HttpPut]
        public async Task<IActionResult> Put([FromBody] UserBean request)
        {
            return await Task.Run(() => {
                using (var c = new MySqlConnection(oConnectionStringsBean.MySQL))
                {
                    var sql = @"UPDATE tbusuarios
                              SET usu_nombre = , usu_passwd, usu_descri, usu_email, usu_estcod WHERE usu_codigo = @usu_codigo";

                    int affetch = c.Execute(sql, request, commandTimeout: 30);

                    return Ok(new { status = true, message = $"{affetch} Registro(s) Insertado(s)" });
                }
            });
        }

    }
}
