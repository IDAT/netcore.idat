﻿using System;
namespace Idat.API.Models
{
    public class UserModel
    {
        public string usu_codigo { get; set; }
        public string usu_nombre { get; set; }
        public string usu_descri { get; set; }
        public string usu_email { get; set; }
        public int usu_estcod { get; set; }
        public string usu_fecreg { get; set; }
        public string usu_imagen { get; set; }
        public string usu_passwd { get; set; }
    }
}
