
using System.Linq;
using System.Net;
using Idat.API.Beans;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using NSwag;
using NSwag.Generation.Processors.Security;

namespace Idat.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;

           
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //TODO - Leer Cadena de Conexion MySQL con Configuration
            //Constants.ConnectionStrings = Configuration.GetValue<string>("ConnectionStrings:MySQL");
            //TODO - Mapper Cadena de Conexion MySQL con Configuration
            ConnectionStringsBean connectionStringsBean = new ConnectionStringsBean();
            Configuration.Bind("ConnectionStrings", connectionStringsBean);
            //------------------------------------------------------------------
            services.AddSingleton(connectionStringsBean);
            //------------------------------------------------------------------
            services.AddControllers();


            //TODO - CORS Allow
            services.AddCors(options =>
            {
                options.AddPolicy("AllowOrigin",
                    builder => builder.AllowAnyOrigin()
                                .WithMethods("GET"));
            });
            //------------------------------------------------------------------


            //TODO -  Add OpenAPI/Swagger document
            //services.AddOpenApiDocument(); // registers a OpenAPI v3.0 document with the name "v1" (default)
            //services.AddSwaggerDocument(); // registers a Swagger v2.0 document with the name "v1" (default)
            //------------------------------------------------------------------

            //TODO -  Add OpenAPI document config
            services.AddOpenApiDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "IDAT NETCore API";
                    document.Info.Description = "ASP.NET Core API + Swagger + OpenAPI ";
                    document.Info.TermsOfService = "None";
                    document.Info.Contact = new NSwag.OpenApiContact
                    {
                        Name = "Alex Torres",
                        Email = string.Empty,
                        Url = "https://twitter.com/superahacker"
                    };
                    document.Info.License = new NSwag.OpenApiLicense
                    {
                        Name = "Use under LICX",
                        Url = "https://example.com/license"
                    };
                };
                config.DocumentName = "OpenAPI 3";
                //config.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT Token"));
                config.OperationProcessors.Add(new AspNetCoreOperationSecurityScopeProcessor("JWT Token")); //-> Replaced the line above with this with no difference
                config.AddSecurity("JWT Token", Enumerable.Empty<string>(),
                    new OpenApiSecurityScheme()
                    {
                        Type = OpenApiSecuritySchemeType.ApiKey,
                        Name = nameof(Authorization),
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Description = "Copy this into the value field: Bearer {token}"
                    }
                );
            });
            //------------------------------------------------------------------


            //TODO -  Add Swagger document config
            /*
            services.AddSwaggerDocument(config =>
            {
                config.PostProcess = document =>
                {
                    document.Info.Version = "v1";
                    document.Info.Title = "IDAT NETCore API";
                    document.Info.Description = "ASP.NET Core API + Swagger + OpenAPI";
                    document.Info.TermsOfService = "None";
                    document.Info.Contact = new NSwag.OpenApiContact
                    {
                        Name = "Alex Torres",
                        Email = string.Empty,
                        Url = "https://twitter.com/superahacker"
                    };
                    document.Info.License = new NSwag.OpenApiLicense
                    {
                        Name = "Use under LICX",
                        Url = "https://example.com/license"
                    };
                };

                config.DocumentName = "OpenAPI 2";
                config.OperationProcessors.Add(new OperationSecurityScopeProcessor("JWT Token"));
                config.AddSecurity("JWT Token", Enumerable.Empty<string>(),
                    new OpenApiSecurityScheme()
                    {
                        Type = OpenApiSecuritySchemeType.ApiKey,
                        Name = nameof(Authorization),
                        In = OpenApiSecurityApiKeyLocation.Header,
                        Description = "Copy this into the value field: Bearer {token}"
                    }
                );
            });
            */
            //------------------------------------------------------------------


        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseHttpsRedirection();
            app.UseRouting();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
            //TODO - LOG4NET
            loggerFactory.AddLog4Net();
            //TODO - Add OpenAPI/Swagger middlewares
            app.UseOpenApi(); // Serves the registered OpenAPI/Swagger documents by default on `/swagger/{documentName}/swagger.json`
            app.UseSwaggerUi3(); // Serves the Swagger UI 3 web ui to view the OpenAPI/Swagger documents by default on `/swagger`
            app.UseReDoc(); // serve ReDoc UI
        }
    }
}
